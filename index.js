import AiService from "./AiService.js";
import fs from "fs"
import ComfyWorkflowService from "./ComfyWorkflowService.js";

const saveFolder = './output'

const {
    ComfyUIURL,
    batchSize,
    generations,
    width,
    height,
    steps,
    scale,
    model,
    sampler,
    scheduler,
    negativePrompt,
    promptTemplate
} = JSON.parse(fs.readFileSync('./settings.json').toString())

// Randomized data pool
const possibleValues = JSON.parse(fs.readFileSync('./placeholders.json').toString())

// Delete existing data
try {
    fs.rmSync(saveFolder, {recursive: true})
} catch {

}

// Create save folder
try {
    fs.mkdirSync(saveFolder)
} catch {

}

function replacePlaceholdersWithRandomValues(prompt, negativePrompt, properties) {
    let newPrompt = String(prompt)
    let newNegPrompt = String(negativePrompt)
    for (const property in properties) {
        const newValue = getRandomValueFromArray(properties[property])
        if (Array.isArray(newValue)) {
            newPrompt = newPrompt.replace(new RegExp("\{\{" + property + "}}",
                'ig'), newValue[0])
            newNegPrompt += `, ${newValue[1]}`
        } else {
            newPrompt = newPrompt.replace(new RegExp("\{\{" + property + "}}",
                'ig'), newValue)
        }
    }

    return {prompt: newPrompt, negPrompt: newNegPrompt}
}

function getRandomValueFromArray(arr) {
    return arr[Math.floor(Math.random() * arr.length)]
}

function extractLoras(str) {
    // Define the regex pattern
    let pattern = /<lora:(.*):(.*):?(.*?)>/g;

    // Initialize an array to hold our findings
    let extractedLoras = [];

    // Use regex .exec method to keep extracting matches from the string
    let match;
    while ((match = pattern.exec(str)) !== null) {
        // Push an object with the matched groups to the array
        // Also, check if strength_clip is provided, else default it to 1.0
        extractedLoras.push({
            filename: match[1],
            strength_model: parseFloat(match[2]),
            strength_clip: match[3] ? parseFloat(match[3]) : 1.0
        });
        str = str.replace(pattern, '')
    }

    // Return the array of lora objects
    return {extractedLoras, newPrompt: str};
}

fs.writeFileSync(`${saveFolder}/_static_settings.json`, JSON.stringify({
    width, height, steps, scale, sampler, scheduler, promptTemplate, negativePrompt
}, null, 2))

let counter = 0
for (let i = 0; i < generations; i++) {
    const {prompt, negPrompt} = replacePlaceholdersWithRandomValues(promptTemplate, negativePrompt, possibleValues)
    const {extractedLoras, newPrompt} = extractLoras(prompt)
    const workflow = ComfyWorkflowService.buildWorkflowWithLoras(newPrompt, negPrompt, Math.floor(Math.random() * 9999999), steps, scale, width, height, model, sampler, scheduler, batchSize, extractedLoras)

    const results = await AiService.generateComfyUIImage(ComfyUIURL, workflow)

    if (results && results.length > 0) {
        for (const result of results) {
            fs.writeFileSync(`${saveFolder}/${counter.toString().padStart(5, '0')}.png`, result)
            fs.writeFileSync(`${saveFolder}/${counter.toString().padStart(5, '0')}.txt`, Buffer.from(`${newPrompt}\n${negPrompt}`))
            counter++
        }
    }
}
