class LoRA {
    /** @type {string} Name of the LoRA file with extension */
    filename

    /** @type {number} Strength of the LoRA */
    strength_model = 1

    /** @type {number} Strength of the LoRA on CLIP */
    strength_clip = 1
}

export default class ComfyWorkflowService {
    /**
     *
     * @param {string} prompt
     * @param {string} negativePrompt
     * @param {number} seed
     * @param {?number} steps
     * @param {?number} cfg
     * @param {?number} width
     * @param {?number} height
     * @param {?string} model
     * @param {?string} sampler
     * @param {?string} scheduler
     * @param {?number} batchSize
     * @param {?LoRA[]} loras
     */
    static buildWorkflowWithLoras(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6.safetensors", sampler = "euler_ancestral", scheduler = "karras", batchSize = 1, loras = []) {
        if (!loras || loras.length === 0) {
            return this.getImageWorkflow(prompt, negativePrompt, seed, steps, cfg, width, height, model, sampler, scheduler, batchSize)
        }

        let lastNode = 4

        const baseWorkflow = {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": sampler,
                    "scheduler": scheduler,
                    "denoise": 1,
                    "model": ["11", 0],
                    "positive": ["6", 0],
                    "negative": ["7", 0],
                    "latent_image": ["5", 0]
                }, "class_type": "KSampler", "_meta": {
                    "title": "KSampler"
                }
            }, "4": {
                "inputs": {
                    "ckpt_name": `${model}`
                }, "class_type": "CheckpointLoaderSimple", "_meta": {
                    "title": "Load Checkpoint"
                }
            }, "5": {
                "inputs": {
                    "width": width, "height": height, "batch_size": batchSize
                }, "class_type": "EmptyLatentImage", "_meta": {
                    "title": "Empty Latent Image"
                }
            }, "6": {
                "inputs": {
                    "text": prompt, "clip": ["11", 1]
                }, "class_type": "CLIPTextEncode", "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            }, "7": {
                "inputs": {
                    "text": negativePrompt, "clip": ["11", 1]
                }, "class_type": "CLIPTextEncode", "_meta": {
                    "title": "CLIP Text Encode (Prompt)"
                }
            }, "8": {
                "inputs": {
                    "samples": ["3", 0], "vae": ["4", 2]
                }, "class_type": "VAEDecode", "_meta": {
                    "title": "VAE Decode"
                }
            }, "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI", "images": ["8", 0]
                }, "class_type": "SaveImage", "_meta": {
                    "title": "Save Image"
                }
            }
        }

        for (let i = 10; i < 10 + loras.length; i++) {
            baseWorkflow[i.toString()] = {
                "inputs": {
                    "lora_name": loras[i - 10].filename,
                    "strength_model": loras[i - 10].strength_model,
                    "strength_clip": loras[i - 10].strength_clip,
                    "model": [lastNode.toString(), 0],
                    "clip": [lastNode.toString(), 1]
                }, "class_type": "LoraLoader", "_meta": {
                    "title": "Load LoRA"
                }
            }
            lastNode = i
        }

        baseWorkflow["3"]["inputs"]["model"] = [lastNode.toString(), 0]
        baseWorkflow["6"]["inputs"]["clip"] = [lastNode.toString(), 1]
        baseWorkflow["7"]["inputs"]["clip"] = [lastNode.toString(), 1]
        return baseWorkflow
    }

    static getImageWorkflow(prompt, negativePrompt, seed, steps = 64, cfg = 5, width = 640, height = 960, model = "realcartoonPixar_v6.safetensors", sampler = "euler_ancestral", scheduler = "karras", batchSize = 1) {
        return {
            "3": {
                "inputs": {
                    "seed": seed,
                    "steps": steps,
                    "cfg": cfg,
                    "sampler_name": sampler,
                    "scheduler": scheduler,
                    "denoise": 1,
                    "model": ["4", 0],
                    "positive": ["6", 0],
                    "negative": ["7", 0],
                    "latent_image": ["5", 0]
                }, "class_type": "KSampler"
            }, "4": {
                "inputs": {
                    "ckpt_name": `${model}`
                }, "class_type": "CheckpointLoaderSimple"
            }, "5": {
                "inputs": {
                    "width": width, "height": height, "batch_size": batchSize
                }, "class_type": "EmptyLatentImage"
            }, "6": {
                "inputs": {
                    "text": prompt, "clip": ["4", 1]
                }, "class_type": "CLIPTextEncode"
            }, "7": {
                "inputs": {
                    "text": negativePrompt, "clip": ["4", 1]
                }, "class_type": "CLIPTextEncode"
            }, "8": {
                "inputs": {
                    "samples": ["3", 0], "vae": ["4", 2]
                }, "class_type": "VAEDecode"
            }, "9": {
                "inputs": {
                    "filename_prefix": "ComfyUI", "images": ["8", 0]
                }, "class_type": "SaveImage"
            }, "10": {
                "inputs": {
                    "vae_name": "vae-ft-mse-840000-ema-pruned.safetensors"
                }, "class_type": "VAELoader"
            }
        }
    }
}
