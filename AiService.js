import axios from 'axios'
import {Buffer} from "node:buffer"
import ComfyWorkflowService from "./ComfyWorkflowService.js";

export default class AiService {
    /**
     *
     * @param {string} URL
     * @param {any} workflow
     * @returns {Promise<*>}
     * @throws {Error}
     */
    static async generateComfyUIImage(URL = "http://localhost:8188/", workflow = null) {
        const response = await axios.post(`${URL}prompt`, {
            prompt: workflow
        }, {
            timeout: 1000 * 60 * 10, headers: {
                'Content-Type': 'application/json'
            }
        }).catch(err=>{
            console.log("Something went wrong!")
        })

        const promptId = response.data?.prompt_id

        let r = null

        async function getRequest(promptId) {
            return new Promise((resolve, reject) => {
                axios.get(`${URL}history/${promptId}`)
                    .then(resp => resolve(resp.data))
                    .catch(reason => reject(reason))
            })
        }

        while (promptId) {
            const result = await getRequest(promptId)
            if (result && Object.keys(result).length > 0) {
                r = result
                break
            } else {
                await new Promise(r => setTimeout(r, 2000))
            }
        }

        const image_filenames = r?.[promptId]?.['outputs']?.['9']?.['images']

        async function fetchFileAndConvertToBuffer(endpointURL) {
            try {
                // Fetch file from the API
                const response = await axios.get(endpointURL, {
                    responseType: 'arraybuffer' // Tell axios to return data as ArrayBuffer
                });

                // Convert the ArrayBuffer to base64
                const base64Data = Buffer.from(response.data, 'binary').toString('base64');

                // Convert base64 to Buffer
                return Buffer.from(base64Data, 'base64');
            } catch (error) {
                console.error('Error fetching and converting file:', error);
                throw error;
            }
        }

        let data = []
        if (image_filenames) {
            for (const image_filename of image_filenames) {
                const filename = image_filename['filename']
                data.push(await fetchFileAndConvertToBuffer(`${URL}view?filename=${filename}`))
            }
        }

        if (!data || data.length === 0) return console.log(`ERROR: no image data was found.`)

        return data.map(d => Buffer.from(d, 'base64'))
    }
}
