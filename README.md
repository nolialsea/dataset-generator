# Semi-randomized Image Generator Script
## Overview
This script is an automated image generator that creates a specified number of semi-random images with AI support. The images created are determined by the instructions given in two key JSON files: `settings.json` and `placeholders.json`.  

---
## SETTINGS.JSON
This file sets the base parameters for the generated images, which include:
- ComfyUIURL - The local URL where the Comfy UI is hosted.
- batchSize - Number of images generated per iteration.
- generations - Total number of iterations (image generations).
- width - Width of the generated image.
- height - Height of the generated image.
- steps - Number of steps in the image generation workflow.
- scale - Scale parameter for the image generation.
- model - Model being used for the image generation. This should be the name of a .safetensors or .ckpt file.
- sampler - Name of the sampler (stochastic dynamics) used in the image generation.
- scheduler - Scheduler for the generator.
- negativePrompt - Base negative prompt. Used to direct the AI away from undesired results.
- promptTemplate - Base template for the positive image prompt. This uses placeholders (like {{person}} or {{background}}) which are replaced with random values at each generation.
---
### Populating SETTINGS.JSON
To set up `settings.json`:
1. Set the appropriate `ComfyUIURL` where your local server runs the Comfy UI.
2. Define the `batchSize` as the number of images you want to generate per generation batch.
3. Define `generations` as the total number of generation batches you want the script to run.
4. Define the `width` and `height` for your desired output image sizes.
5. Set `steps`, `scale`, `model`, `sampler`, and `scheduler` according to your specific needs and available resources.
6. Leave `negativePrompt` empty if you have no specific aspects you want to avoid in the image generation.
7. Set your `promptTemplate`. This is basically the structure of your image prompt, using placeholder brackets (like {{placeholder}}) that the script will replace with random values from `placeholders.json` at each generation.
---
## PLACEHOLDERS.JSON
### Structure
The `placeholders.json` file is where you define all possible values for each placeholder that you have used in your `settings.json` promptTemplate.
### Populating PLACEHOLDERS.JSON
To set up `placeholders.json`:
1. Craft your values for each placeholder you have in your promptTemplate. These need to be arrays even if there`s only one value.
2. These values can be simple strings like a name (`"emma watson"`) or a more complex item including a LORA string and a negative prompt (`["in (skyrim:1.25) unreal engine <lora:skyrim.safetensors:0.9>", "photography, photo"]`).
---
## THE SCRIPT
The script is designed to work autonomously, running through specified generations and creating images according to the settings provided. It primarily works in three main steps:
1. **Preparation:**: It prepares the settings and the placeholder data pool.
2. **Randomization:**: It goes through each generation, replacing placeholders with random values from the placeholder pool.
3. **Image Generation:**: It then sends this to the AI, which returns the generated images that are saved in the designated folder.
