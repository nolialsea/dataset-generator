export default class Utils {
    static async retryFunction(func, args = [], maxRetries = 3) {
        let retries = 0;
        let result;
        while (!result && retries < maxRetries) {
            result = await func(...args);
            retries++;
            if (!result) await Utils.sleep(10000)
        }
        return result;
    }

    static sleep(ms) {
        // Everybody needs a little rest, don't we?
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
